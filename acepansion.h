/*
** willy.acepansion public API
*/

#ifndef ACEPANSION_H
#define ACEPANSION_H


#define LIBNAME "willy.acepansion"
#define VERSION 1
#define REVISION 3
#define DATE "23.04.2024"
#define COPYRIGHT "� 2021-2024 Philippe Rimauro"

#define API_VERSION 7


#endif /* ACEPANSION_H */

