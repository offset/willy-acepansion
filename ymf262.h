/*
**
** Software implementation of YMF262 FM sound generator type OPL3
** Copyright Jarek Burczynski
** Version 0.2
**
** Converted to plain C by Philippe 'OffseT' Rimauro
**  - Removed dead code
**  - Removed compilation warnings
**  - Adapted public API
**
**

Revision History:

03-03-2003: initial release
 - thanks to Olivier Galibert and Chris Hardy for YMF262 and YAC512 chips
 - thanks to Stiletto for the datasheets

   Features as listed in 4MF262A6 data sheet:
    1. Registers are compatible with YM3812 (OPL2) FM sound source.
    2. Up to six sounds can be used as four-operator melody sounds for variety.
    3. 18 simultaneous melody sounds, or 15 melody sounds with 5 rhythm sounds (with two operators).
    4. 6 four-operator melody sounds and 6 two-operator melody sounds, or 6 four-operator melody
       sounds, 3 two-operator melody sounds and 5 rhythm sounds (with four operators).
    5. 8 selectable waveforms.
    6. 4-channel sound output.
    7. YMF262 compabile DAC (YAC512) is available.
    8. LFO for vibrato and tremolo effedts.
    9. 2 programable timers.
   10. Shorter register access time compared with YM3812.
   11. 5V single supply silicon gate CMOS process.
   12. 24 Pin SOP Package (YMF262-M), 48 Pin SQFP Package (YMF262-S).


differences between OPL2 and OPL3 not documented in Yamaha datahasheets:
- sinus table is a little different: the negative part is off by one...

- in order to enable selection of four different waveforms on OPL2
  one must set bit 5 in register 0x01(test).
  on OPL3 this bit is ignored and 4-waveform select works *always*.
  (Don't confuse this with OPL3's 8-waveform select.)

- Envelope Generator: all 15 x rates take zero time on OPL3
  (on OPL2 15 0 and 15 1 rates take some time while 15 2 and 15 3 rates
  take zero time)

- channel calculations: output of operator 1 is in perfect sync with
  output of operator 2 on OPL3; on OPL and OPL2 output of operator 1
  is always delayed by one sample compared to output of operator 2


differences between OPL2 and OPL3 shown in datasheets:
- YMF262 does not support CSM mode

*/

#ifndef __YMF262_H__
#define __YMF262_H__

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif


typedef VOID (*OPL3_TIMERHANDLER)(APTR param,int timer,int period);
typedef VOID (*OPL3_IRQHANDLER)(APTR param,int irq);
typedef VOID (*OPL3_UPDATEHANDLER)(APTR param,int min_interval_us);


APTR ymf262_init(int clock, int rate);
VOID ymf262_shutdown(APTR chip);
VOID ymf262_reset_chip(APTR chip);
BOOL ymf262_write(APTR chip, int a, int v);
UBYTE ymf262_read(APTR chip, int a);
BOOL ymf262_timer_over(APTR chip, int c);
VOID ymf262_get_audio(APTR chip, SHORT *ch_a, SHORT *ch_b, SHORT *ch_c, SHORT *ch_d);

VOID ymf262_set_timer_handler(APTR chip, OPL3_TIMERHANDLER TimerHandler, APTR param);
VOID ymf262_set_irq_handler(APTR chip, OPL3_IRQHANDLER IRQHandler, APTR param);
VOID ymf262_set_update_handler(APTR chip, OPL3_UPDATEHANDLER UpdateHandler, APTR param);


#endif /* __YMF262_H__ */
