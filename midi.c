#include "midi.h"

#ifdef __amigaos__

/*
** We direclty map MIDI I/O to the camd.library so that virtually
** any MIDI hardware or software synthetizer could be used
*/

#include <clib/alib_protos.h>
#include <clib/debug_protos.h>

#include <proto/exec.h>
#include <proto/camd.h>

#include <acepansion/lib_header.h>
#include <libraries/camd.h>



struct MidiHandler
{
    UBYTE buffer[10];
    ULONG bufferIndex;

    struct Library *CamdBase;
    struct MidiNode *node;
    struct MidiLink *out;
};



APTR midi_init(CONST_STRPTR clusterName)
{
    BOOL done = FALSE;
    struct MidiHandler *midi = AllocVec(sizeof(struct MidiHandler), MEMF_PUBLIC|MEMF_CLEAR);

    if(midi)
    {
        if((midi->CamdBase = OpenLibrary("camd.library", 50L)))
        {
            struct Library *CamdBase = midi->CamdBase;

            midi->node = CreateMidi(
                MIDI_MsgQueue, 2048L,
                MIDI_SysExSize, 8512L,
                TAG_END);

            if(midi->node)
            {
                midi->out = AddMidiLink(midi->node, MLTYPE_Sender,
                    MLINK_Location, (IPTR)clusterName,
                    TAG_END);
                done = TRUE;
            }
        }
    }

    if(!done)
    {
        midi_shutdown(midi);
        midi = NULL;
    }

    return midi;
}

VOID midi_shutdown(APTR m)
{
    struct MidiHandler *midi = (struct MidiHandler *)m;

    if(midi)
    {
        if(midi->CamdBase)
        {
            struct Library *CamdBase = midi->CamdBase;

            if(midi->node)
            {
                if(midi->out)
                {
                    midi_reset(m);
                    RemoveMidiLink(midi->out);
                }
                DeleteMidi(midi->node);
            }
            CloseLibrary(CamdBase);
        }
        FreeVec(midi);
    }
}

void midi_reset(APTR m)
{
    struct MidiHandler *midi = (struct MidiHandler *)m;

    if(midi)
    {
        struct Library *CamdBase = midi->CamdBase;
        MidiMsg mm;
        int chan;

        for(chan=0; chan<16; chan++)
        {
            // All sounds off
            mm.mm_Status = MS_Ctrl | chan;
            mm.mm_Data1  = 120;
            mm.mm_Data2  = 0;
            PutMidiMsg(midi->out, &mm);

            // Reset all controllers
            mm.mm_Status = MS_Ctrl | chan;
            mm.mm_Data1  = 121;
            mm.mm_Data2  = 0;
            PutMidiMsg(midi->out, &mm);

            // All notes off
            mm.mm_Status = MS_Ctrl | chan;
            mm.mm_Data1  = 123;
            mm.mm_Data2  = 0;
            PutMidiMsg(midi->out, &mm);
        }
        midi->bufferIndex = 0;
    }
}

void midi_write(APTR m, UBYTE byte)
{
    struct MidiHandler *midi = (struct MidiHandler *)m;

    if(midi)
    {
        struct Library *CamdBase = midi->CamdBase;
        BOOL putMidi = FALSE;

        if(midi->bufferIndex >= sizeof(midi->buffer))
        {
            kprintf("Willy/S2P: buffer overflow!\n");
            midi->bufferIndex = 1;
        }
        midi->buffer[midi->bufferIndex++] = byte;

        switch(midi->buffer[0] & MS_StatBits)
        {
            // MIDI commands with two parameters
            case MS_Prog:
            case MS_ChanPress:
                putMidi = (midi->bufferIndex == 2);
                break;
            // MIDI commands with three parameters
            case MS_NoteOff:
            case MS_NoteOn:
            case MS_PolyPress:
            case MS_Ctrl:
            case MS_PitchBend:
                putMidi = (midi->bufferIndex == 3);
                break;
            // Sysex MIDI command
            case MS_SysEx:
                if(byte == MS_EOX)
                {
                    PutSysEx(midi->out, midi->buffer);
                    midi->bufferIndex = 0;
                }
                break;
        }

        if(putMidi)
        {
            MidiMsg mm;

            mm.mm_Status = midi->buffer[0];
            mm.mm_Data1  = midi->buffer[1];
            mm.mm_Data2  = midi->buffer[2];
            PutMidiMsg(midi->out, &mm);
            midi->bufferIndex = 0;
        }
    }
}

#elif defined(__HAIKU__)

#include <MidiSynth.h>

#include <stdio.h>



static unsigned char midiBuffer[10];
static int bufferIndex;

BMidiSynth synth;



APTR midi_init(CONST_STRPTR clusterName)
{
    be_synth->LoadSynthData(B_BIG_SYNTH);
    synth.EnableInput(true, true);

    // Dummy...
    return &synth;
}

VOID midi_shutdown(APTR handler)
{
}

VOID midi_reset(APTR handler)
{
    bufferIndex = 0;

    synth.AllNotesOff(false);
}

VOID midi_pushdata(APTR handler, UBYTE byte)
{
    if (byte & 0x80) {
        if (byte == 0xF7) {
            // Special case: end of SysEx command
            if ((bufferIndex == 10)
                && (midiBuffer[0] == 0xF0)
                && (midiBuffer[1] == 0x41)
                && (midiBuffer[2] == 0x10)
                && (midiBuffer[3] == 0x42)
                && (midiBuffer[4] == 0x12)
                && (midiBuffer[5] == 0x40)
                && (midiBuffer[6] == 0x00)
                && (midiBuffer[7] == 0x7f)
                && (midiBuffer[8] == 0x00)
                && (midiBuffer[9] == 0x41)) {
                // Roland General Sound reset
                synth.AllNotesOff(false);
            } else {
                printf("Unknown or invalid sysex!\n");
            }
        }

        // Start of a new command
        bufferIndex = 0;
        midiBuffer[bufferIndex++] = byte;

    } else {
        if (bufferIndex >= sizeof(midiBuffer)) {
            puts("MIDI BUFFER OVERFLOW");
            bufferIndex = 1;
        }
        midiBuffer[bufferIndex++] = byte;

        unsigned char channel = (midiBuffer[0] & 0x0f) + 1;
            // + 1 because Haiku APIs counts MIDI channels from 1 to 16 :(

        switch(midiBuffer[0] & 0xF0) {
            case 0x80:
                if (bufferIndex == 3) {
                    synth.NoteOff(channel, midiBuffer[1], midiBuffer[2]);
                }
                break;
            case 0x90:
                if (bufferIndex == 3) {
                    synth.NoteOn(channel, midiBuffer[1], midiBuffer[2]);
                }
                break;
            case 0xA0:
                if (bufferIndex == 3) {
                    // "Aftertouch"
                    synth.KeyPressure(channel, midiBuffer[1], midiBuffer[2]);
                }
                break;
            case 0xB0:
                if (bufferIndex == 3) {
                    synth.ControlChange(channel, midiBuffer[1], midiBuffer[2]);
                }
                break;
            case 0xC0:
                if (bufferIndex == 2) {
                    synth.ProgramChange(channel, midiBuffer[1]);
                }
                break;
            case 0xD0:
                if (bufferIndex == 2) {
                    synth.ChannelPressure(channel, midiBuffer[1]);
                }
                break;
            case 0xE0:
                if (bufferIndex == 3) {
                    synth.PitchBend(channel, midiBuffer[1], midiBuffer[2]);
                }
                break;
            case 0xF0:
                // Sysex data of some kind, we'll handle it when receiving the F7 opcode
                break;
        }
    }
}

#else

#error FixMe!

#endif
