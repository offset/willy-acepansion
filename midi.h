#ifndef __MIDI_H__
#define __MIDI_H__

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif

APTR midi_init(CONST_STRPTR clusterName);
VOID midi_shutdown(APTR handler);
VOID midi_reset(APTR handler);
VOID midi_write(APTR handler, UBYTE byte);

#endif /* __MIDI_H__ */
